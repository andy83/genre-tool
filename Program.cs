﻿using System;
using System.IO;
using System.Linq;
using System.Xml;


class GenreTool 
{
    public static void Main(string[] args) 
    {
        string mainPath = args.First();
        string[] folderPaths = GetFolderPaths(mainPath);
        foreach(var path in folderPaths)
        {
            string[] files = GetFilePaths(path);
            string genreName = GetGenreName(path);
            foreach(var file in files)
            {
                setGenres(file, genreName);
            }
            
        }
    }

    static string[] GetFolderPaths(string rootPath)
    {
        string[] folderPaths = Directory.GetDirectories(rootPath, "*", SearchOption.AllDirectories);
        return folderPaths;
    }

    static string[] GetFilePaths(string folderPath)
    {
        string[] fileNames = Directory.GetFiles(folderPath, "*.nfo", SearchOption.AllDirectories);
        return fileNames;
    }

    static string GetGenreName(string path)
    {
        string[] pathArray = path.Split(Path.DirectorySeparatorChar);
        
        string genre = pathArray[pathArray.Length - 2]; 
        
        return genre;
    }

    static void setGenres(string file, string genreName) 
    {   
        XmlDocument doc = new XmlDocument();
        doc.Load(file);
        XmlNodeList genreNodes = doc.SelectNodes("//movie/genre");        
        
        int genreCount = genreNodes.Count - 1;

        foreach (XmlNode item in genreNodes)
        {    
            if(genreCount < 1) 
            {                
                XmlElement genre = doc.CreateElement("genre");
                genre.InnerText = genreName;
                genreNodes[genreCount].ParentNode.ReplaceChild(genre, genreNodes[genreCount]);                
            }
            else if (genreCount >= 1)
            {
                genreNodes[genreCount].ParentNode.RemoveChild(genreNodes[genreCount]);
                genreCount --;
            }
            
        }
        doc.Save(file);
    }
}
