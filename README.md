### Movie Genre Tool

A tool for changing the xml stored genre to one that matches the folder the movie is in.
The structure must match

..\genreName\moviefolder\movieData.nfo

To use the tool enter the path to the folder that your movie collection is stored in, into the console, for example

c:\users\username\videos

The tool will change every .nfo file for every film found at the path provided.